import React, { useEffect, useState } from "react";

import Category from "./catalog/Category";
import Cart from "./cart/Cart";

import { useGoods } from "./custom-hooks/useGoods";
import { useNames } from "./custom-hooks/useNames";
import { useCurrencyRate } from "./custom-hooks/useCurrencyRate";

import "./shop-main.css";

function ShopMain() {
  // state
  const goods = useGoods();
  const names = useNames();
  const currencyRate = useCurrencyRate();
  const [categories, setCategories] = useState({});
  const [cartProducts, setCartProducts] = useState({});

  // METHODS
  const removeFromCart = (cartProductsKey) => {
    setCartProducts((cartProducts) => {
      const { [cartProductsKey]: _, ...newCartProducts } = cartProducts;
      return newCartProducts;
    });
  };
  const addToCartHandler = (product) => {
    setCartProducts((prevCartProducts) => {
      // This state hook fires twice in development mode? This is a feature of React's strict mode (no, it's not a bug). В режиме разработки этот колбэк запускается дважды ЭТО НЕ БАГ Это фича рефкта :-) почитать можно тут https://reactjs.org/docs/strict-mode.html
      const newCartProducts = Object.assign({}, prevCartProducts);
      if (product.groupId && product.id) {
        if (newCartProducts[`${product.groupId}_${product.id}`]) {
          if (
            newCartProducts[`${product.groupId}_${product.id}`].qty + 1 <=
            product.qty
          ) {
            newCartProducts[`${product.groupId}_${product.id}`].qty += 1;
          } else {
            return prevCartProducts;
          }
        } else {
          newCartProducts[`${product.groupId}_${product.id}`] = {
            qty: 1,
            product,
          };
        }
      }
      return newCartProducts;
    });
  };

  const prepareCategories = () => {
    const newCategories = {};
    if (goods && names) {
      goods.forEach((good) => {
        const getPreviousPrice = () => {
          if (categories[good.G]) {
            const oldGood = categories[good.G].goods.find(
              (product) => product.id === good.T
            );
            if (oldGood) {
              return oldGood.roublePrice;
            }
          }
          return Math.round(good.C * currencyRate * 100) / 100;
        };
        const newGood = {
          groupId: good.G,
          id: good.T,
          name: names[good.G]["B"][good.T]["N"],
          qty: good.P,
          dollarPrice: good.C,
          previousRoublePrice: getPreviousPrice(),
          roublePrice: Math.round(good.C * currencyRate * 100) / 100,
          // I am lazy :-) and just put is here because i don't want to create store for this simple project
          addToCart: () => {
            addToCartHandler(newGood);
          },
        };
        newCategories[good.G] = {
          name: names[good.G]["G"],
          goods: [
            ...(newCategories[good.G]
              ? [...newCategories[good.G].goods, newGood]
              : [newGood]),
          ],
        };
      });
    }
    setCategories(newCategories);
  };

  // EFFECTS
  useEffect(() => {
    prepareCategories();
    // eslint-disable-next-line
  }, [names, goods, currencyRate]);

  // JSX
  return (
    <div className="main-shop-container">
      Test React Shop
      <hr />
      курс: {currencyRate} руб за 1 $
      <hr />
      {Object.keys(cartProducts).length > 0 ? (
        <Cart cartProducts={cartProducts} removeFromCart={removeFromCart} />
      ) : (
        "Корзина пуста"
      )}
      <div className="categories-container">
        {Object.keys(categories).map((key, index) => (
          <Category key={`category_${index}`} category={categories[key]} />
        ))}
      </div>
    </div>
  );
}

export default ShopMain;
