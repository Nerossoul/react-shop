import React from "react";
import PropTypes from "prop-types";

import "./product.css";

Product.propTypes = {
  product: PropTypes.object.isRequired,
};

function Product({ product }) {
  return (
    <div className="product-container">
      <div>
        {product.name} ({product.qty})
      </div>
      <div className="product-price">
        <div
          className={
            product.roublePrice >= product.previousRoublePrice
              ? "product-price_red"
              : "product-price_green"
          }
        >
          {product.roublePrice}
          <br />
          <button onClick={product.addToCart}>+</button>
        </div>
      </div>
    </div>
  );
}

export default Product;
