import React from "react";
import PropTypes from "prop-types";

import Product from "./Product";

import "./category.css";

Category.propTypes = {
  category: PropTypes.object.isRequired,
};

function Category({ category }) {
  return (
    <div className="category-container">
      <div style={{ backgroundColor: "#ddd", padding: "8px" }}>
        {category.name}
      </div>
      <div className="products-container">
        {category.goods.map((product, index) => (
          <Product key={`product_${index}`} product={product} />
        ))}
      </div>
    </div>
  );
}

export default Category;
