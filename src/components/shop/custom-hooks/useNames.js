import {useEffect, useState} from "react";
import Api from "../../../api";

export const useNames = () => {
    const [ names , setNames] = useState(null)


    const getNames = async (isRequestActual)=> {
        if (isRequestActual.state) {
            let result =  await Api.getNames()
            if (result) {
                if (isRequestActual.state) {
                    setNames(result)
                }
            }
        }
        return null
    }
    useEffect(()=>{
        const isRequestActual = { state: true}
        getNames(isRequestActual)
        return ()=> {
            isRequestActual.state = false
        }
    },[])
    return names
}