import {useEffect, useState} from "react";
import Api from "../../../api";

export const useGoods = () => {
    const [ goods , setGoods] = useState(null)

    const getGoods = async (isRequestActual)=> {
        if (isRequestActual.state) {
            let {Success, Value} =  await Api.getData()
            if (Success) {
                const {Goods} = Value
                if (isRequestActual.state) {
                    setGoods(Goods)
                }
            }
        }
        return null
    }
    useEffect(()=>{
        const isRequestActual = { state: true}
        getGoods(isRequestActual)

        return ()=> {
            isRequestActual.state = false
        }
    },[])
    return goods
}