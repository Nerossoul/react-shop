import { useEffect, useState } from "react";
import Api from "../../../api";

export const useCurrencyRate = () => {
  const [currencyRate, setCurrencyRate] = useState(null);

  const getCurrentcyRate = async (isRequestActual) => {
    if (isRequestActual.state) {
      let rate = await Api.getCurrencyRate();
      if (rate >= 20 && rate <= 80)
        if (isRequestActual.state) {
          setCurrencyRate(rate);
        }
    }
    return null;
  };
  useEffect(() => {
    const isRequestActual = { state: true };
    getCurrentcyRate(isRequestActual);
    return () => {
      isRequestActual.state = false;
    };
  }, []);

  useEffect(() => {
    const isRequestActual = { state: true };

    const timeoutId = setTimeout(
      (isRequestActual) => getCurrentcyRate(isRequestActual),
      15000,
      isRequestActual
    );

    return () => {
      clearTimeout(timeoutId);
      isRequestActual.state = false;
    };
  }, [currencyRate]);

  return Math.floor(currencyRate * 100) / 100;
};
