import React from "react";
import PropTypes from "prop-types";

import "./cart.css";

Cart.propTypes = {
  cartProducts: PropTypes.object.isRequired,
  removeFromCart: PropTypes.func.isRequired,
};

function Cart({ cartProducts, removeFromCart }) {
  const getTotal = () => {
    return Object.keys(cartProducts).reduce((total, cartProductKey) => {
      const { qty, product } = cartProducts[cartProductKey];
      const newTotal = total + qty * product.roublePrice;
      return Math.round(newTotal * 100) / 100;
    }, 0);
  };
  return (
    <div>
      <table className="cart-table">
        <thead>
          <tr className="cart-table__row-bordered">
            <th>Наименование</th>
            <th>Количество</th>
            <th>Цена</th>
            <th> </th>
          </tr>
        </thead>
        <tbody>
          {Object.keys(cartProducts).map((cartProductKey, index) => {
            const { qty, product } = cartProducts[cartProductKey];
            return (
              <tr key={`row_${index}`} className="cart-table__row-bordered">
                <td>{product.name}</td>
                <td>
                  {qty} шт.
                  {qty === product.qty ? (
                    <div className="cart-table__label">
                      Количество <br /> ограничено
                    </div>
                  ) : (
                    ""
                  )}
                </td>
                <td>{product.roublePrice} руб/шт</td>
                <td>
                  <button onClick={() => removeFromCart(cartProductKey)}>
                    удалить
                  </button>
                </td>
              </tr>
            );
          })}
          <tr>
            <td colSpan={2}>
              <span className="cart-table__note">
                При добавлении в корзину цена фиксируется, у нас же публичная
                оферта мы не хотим чтоб цена поменялась пока покупатель идет к
                кассе :-)
              </span>
            </td>
            <td>
              <h3>{getTotal()}</h3>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  );
}

export default Cart;
