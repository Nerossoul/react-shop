import axios from "axios";

class Api {
  constructor() {
    this.apiClient = axios.create({
      baseURL: "data/",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
    });
  }

  async getData() {
    const result = await this.apiClient.get("data.json");
    if (result.data) {
      return result.data;
    }
    throw new Error("Something goes wrong!");
  }
  async getNames() {
    const result = await this.apiClient.get("names.json");
    if (result.data) {
      return result.data;
    }
    throw new Error("Something goes wrong!");
  }
  async getCurrencyRate() {
    const MIN = 20;
    const MAX = 80;
    return MIN + Math.random() * (MAX - MIN);
  }
}
export default new Api();
