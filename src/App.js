import React from 'react';

import ShopMain from "./components/shop";

import './App.css';

function App() {
  return (
    <div className="App">
      <ShopMain />
    </div>
  );
}

export default App;